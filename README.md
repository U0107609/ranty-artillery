# ranty-artillery

Proyecto para realizar pruebas en ranty

## Pasos
1 - Instalar de manera Global Artillery.

2 - Editar las rutas y puntos de entrada de acuerdo al ambiente en que se quiera realizar la prueba, del archivo de variables de entorno .env en la raiz del Proyecto.

3 - Editar las rutas dentro de cada uno de los circuitos de pago en yml, alojados en la carpeta config, coincidiendo con las rutas del .env .

4 - finalmente posicionarse en la carpera config y correr el siguiente comando "artillery run (NOMBRE DEL CIRCUITO).yml -e .env"
