require('dotenv').config()
const { promises: fs } = require('fs')
const path = require('path')
const axios = require('axios')

async function getAccessToken (context, ee, next) {  
  const response = await axios.post(process.env.B2B_AUTH_ENDPOINT, {
    client_id: process.env.B2B_CLIENT_ID,
    client_secret: process.env.B2B_CLIENT_SECRET,
    audience: process.env.B2B_CLIENT_AUDIENCE,
    // grant_type: 'client_credentials',
    cache: true
  })

 context.vars.accessToken = response.data.access_token
  return next()
}

async function loadJson (filename) {
  const content = await fs.readFile(path.join(__dirname, '..', 'data', filename))
  const json = JSON.parse(content)
  return json
}

async function loadPaymentRequestBody (request, context, ee, next) {
  context.vars.paymentRequestBody = await loadJson('payment-request.body.json')
  return next()
}

module.exports = {
  getAccessToken,
  loadPaymentRequestBody
}
